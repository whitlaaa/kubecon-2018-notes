# kubecon-2018-notes

The `day1-3` files contain some hastily compiled notes dumped from a combination of memory and Google Keep (I don't like taking
tons of notes during a talk since it's distracting).

There are gaps where I went to a session, but didn't feel that the speaker brought any more insight than the slides
already contained.

## Tools mentioned

There were approximately 18,000 tools/projects mentioned at the conference. Here's a small (probably incomplete) list of them
for further research later:

* [Flagger](https://github.com/stefanprodan/flagger) - Istio canary operator; discussed with Weaveworks folks at booth
* [OPA - Open Policy Agent](https://www.openpolicyagent.org/)
* [Grafana Loki](https://grafana.com/loki) - announced at kubecon
* [skaffold](https://skaffold.dev/)
* [draft](https://draft.sh/)
* [keel](https://keel.sh/) - another gitops project
* vessel - chick-fil-a
* [kubescope-cli](https://github.com/hharnisc/kubescope-cli) - barebones resource visualization in CLI
* [kubectl-trace](https://github.com/fntlnz/kubectl-trace)
* [cluster-api](https://github.com/kubernetes-sigs/cluster-api)
* grafana design doc + issue for gitops dashboard config management
* FluxHelmRelease values via secrets - recent release
* [dive](https://github.com/wagoodman/dive) - see detailed info about image layers
* [cnab](https://cnab.io/) - cloud native app bundles
* [auger](https://github.com/jpbetz/auger) - etcd decoding
* diana (Kelsey Hightower's awesome Diana Ross CLI)
* [m3db](https://github.com/m3db) - Uber's TSDB
* boltdb - embedded by etcd; now under [bbolt](https://github.com/etcd-io/bbolt)
* fleet - chick-fil-a
* hooves-up - chick-fil-a
* [skycfg](https://github.com/stripe/skycfg) - Julia Evans mentioned using this for Stripe's yaml wrapper
* atlas - chick-fil-a
* [kubegen](https://github.com/errordeveloper/kubegen) - create clusters/config
* "k tool" from airbnb - wraps a bunch of kubectl commands
* [squash](https://github.com/solo-io/squash) - debugging tool

## Some must-watch/reads

There were a ton of great sessions. This list (which will be added to as I go back through stuff) is just some of the talks or
resources I felt were definite must watches.

* Julia Evans - High reliability infrastructure migration keynote
* Phippy goes to the zoo - children's book from MS
* The future of humans in a world of automation - lightning talk by Kendall Miller of ReactiveOps
* Kelsey's keynote
* Istio packet's eye view - good visual intro to istio traffic
* OPA intro/deep dive - good sampling of what it can do; possible ideas for usage at IEI

## Resources

* [YouTube playlist of all sessions](https://www.youtube.com/playlist?list=PLj6h78yzYM2PZf9eA7bhWnIh_mK1vyOfU)
* [KubeCon/CloundNativeCon NA 2018 site](https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2018/)